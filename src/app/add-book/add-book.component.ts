import { BooksService } from './../books.service';
import { BooksComponent } from './../books/books.component';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  title:string;
  author:string;

  constructor(private router: Router, private bService: BooksService) {}

  onSubmit(){
    this.bService.addBooks(this.title,this.author);
    this.router.navigate(['/books']);
  }

  ngOnInit() {
  }

}
