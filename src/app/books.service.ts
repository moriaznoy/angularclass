import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}];

  /*
  getBooks(){
    const booksObservable = new Observable(
      observer => {
        setInterval(
         () => observer.next(this.books),5000
        )
      }
    )
    return booksObservable;
  }
  */

  getBooks():Observable<any[]> {
    return this.db.collection('books').valueChanges()
  }

  addBooks(title:string,author:string){
    const book = {title:title,author:author}
    this.db.collection('books').add(book);
  }

  /*
  getBooks(){
    setInterval(()=>this.books,1000)
  }
  */
  constructor(private db:AngularFirestore) {}
}
