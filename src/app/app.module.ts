import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import { FormsModule }   from '@angular/forms';
import { MatInputModule, MatSelectModule } from '@angular/material';

import { BooksComponent } from './books/books.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';

import { RouterModule, Routes } from '@angular/router';
import { TempformComponent } from './tempform/tempform.component';
import { HttpClientModule } from '@angular/common/http';


import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { AddBookComponent } from './add-book/add-book.component';


const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'temperatures', component: TemperaturesComponent},
  // { path: 'temperatures/:temperature/:city', component: TemperaturesComponent},
  { path: 'temperatures/:city', component: TemperaturesComponent},
  { path: 'tempform', component: TempformComponent},
  { path: 'add-book', component: AddBookComponent},
  { path: '',
    redirectTo: '/books',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NavComponent,
    BooksComponent,
    TemperaturesComponent,
    TempformComponent,
    AddBookComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    FormsModule,
    MatCardModule, 
    MatInputModule,
    MatSelectModule,
    HttpClientModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig,'class-app'),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } 
    )    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
