import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  panelOpenState = false;
  books: any;  
  books$:Observable<any>;

  constructor(private booksservice:BooksService) { }

  ngOnInit() {
    /*
    this.books = this.booksservice.getBooks().subscribe(
      (books) => this.books = books
    )
    */
   this.books$ = this.booksservice.getBooks();
  }

}
